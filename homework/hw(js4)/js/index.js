"use strict";
function math(number1 = 0, number2 = 0, symbol, res = 0) {
  do {
    number1 = prompt("number1");
  } while (isNaN(number1) || number1 === null || number1 === "");
  do {
    number2 = prompt("number2");
  } while (isNaN(number2) || number2 === null || number2 === "");
  do {
    symbol = prompt("symbol");
  } while (
    symbol !== "+" &&
    symbol !== "-" &&
    symbol !== "*" &&
    symbol !== "/"
  );
  if (symbol === "+") {
    res = +number1 + +number2;
  } else if (symbol === "-") {
    res = number1 - number2;
  } else if (symbol === "*") {
    res = number1 * number2;
  } else if (symbol === "/") {
    res = number1 / number2;
  }
  alert(res);
  return;
}
// math();
