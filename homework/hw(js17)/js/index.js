"use strict";
// ### Дане завдання не обов'язкове для виконання

// ## Завдання

// Створити об'єкт "студент" та проаналізувати його табель. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - Створити порожній об'єкт `student`, з полями `name` та `lastName`.
// - Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
// - У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента `tabel`.
// - порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
// - Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення `Студенту призначено стипендію`.

// #### Література:
// - [Об'єкти як асоціативні масиви](https://learn.javascript.ru/object)
// - [Перебір властивостей об'єктів](https://learn.javascript.ru/object-for-in)

const student = {
  name: prompt("Imya"),
  lastName: prompt("Familiya"),
  tabel: {},
  newKey(predmet, otsinka) {
    if (predmet in student.tabel) {
      console.log("Bylo");
    } else {
      student.tabel[predmet] = otsinka;
    }
  },
};
let predmet = "";
let otsinka = 0;
do {
  predmet = prompt("predmet");
  otsinka = prompt("otsinka");
  student.newKey(predmet, otsinka);
} while (predmet && otsinka);
delete student.tabel.null;
console.log(student.name);
console.log(student.lastName);
// console.log(student);
console.log("tabel: ");
for (const key in student.tabel) {
  console.log( key + " = " + student.tabel[key]);
}
