"use strict";
// ## Теоретичні питання
// 1. Опишіть своїми словами, що таке метод об'єкту
// 2. Який тип даних може мати значення властивості об'єкта?
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?

// ## Завдання

// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Написати функцію `createNewUser()`, яка буде створювати та повертати об'єкт `newUser`.
// - При виклику функція повинна запитати ім'я та прізвище.
// - Використовуючи дані, введені юзером, створити об'єкт `newUser` з властивостями `firstName` та `lastName`.
// - Додати в об'єкт `newUser` метод `getLogin()`, який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, `Ivan Kravchenko → ikravchenko`).
// - Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію `getLogin()`. Вивести у консоль результат виконання функції.

// #### Необов'язкове завдання підвищеної складності

// - Зробити так, щоб властивості `firstName` та `lastName` не можна було змінювати напряму. Створити функції-сеттери `setFirstName()` та `setLastName()`, які дозволять змінити дані властивості.

function createNewUser() {
  let firstName = prompt("Input your first name");
  let lastName = prompt("Input your last name");
  const newUser = {
    firstName,
    lastName,
    getLogin() {
      return (
        this.firstName.toLowerCase().substring(0, 1) +
        this.lastName.toLowerCase()
      );
    },
  };
  Object.defineProperty(newUser, firstName, {
    writable: false,
  });
  return newUser;
}
let user1 = createNewUser();
user1.lastName = "asdads";
console.log(user1.getLogin());
// console.log(createNewUser().getLogin());
