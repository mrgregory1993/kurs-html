"use strict";
// ## Теоретичні питання

// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// 2. Які засоби оголошення функцій ви знаєте?
// 3. Що таке hoisting, як він працює для змінних та функцій?

// ## Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
//   1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) і зберегти її в полі `birthday`.
//   2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
//   3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
// - Вивести в консоль результат роботи функції `createNewUser()`, а також функцій `getAge()` та `getPassword()` створеного об'єкта.

function createNewUser(firstName, lastName, userAge) {
  firstName = prompt("Input your first name");
  lastName = prompt("Input your last name");
  userAge = prompt("Input your bitrhday");
  const newUser = {
    Name1: firstName,
    Name2: lastName,
    today: new Date(),
    bitrhday: new Date(userAge.split(".").reverse().join(".")),
    // getAge() {
    //     return parseInt(
    //         Math.abs(this.today - this.bitrhday) / (24 * 3600 * 1000 * 365.25)
    //       );
    //     },
    getAge() {
      return Math.floor(
        (this.today - this.bitrhday) / (24 * 3600 * 1000 * 365.25)
      );
    },
    getPassword() {
      return (
        this.Name1.toUpperCase().substring(0, 1) +
        this.Name2.toLowerCase() +
        this.bitrhday.getFullYear()
      );
    },

    getLogin() {
      return (
        this.Name1.toLowerCase().substring(0, 1) + this.Name2.toLowerCase()
        );
      },
    };
  return newUser;
}
let user = createNewUser();
console.log(user.getAge());
// console.log(user.getPassword());
console.log(user.getLogin());


// var date1 = new Date();
// var date2 = new Date("1/24/2023");

// var diff = date2 - date1;

// var milliseconds = diff;
// var seconds = milliseconds / 1000;
// var minutes = seconds / 60;
// var hours = minutes / 60;
// var days = hours / 24;

// alert(days);
// alert(Math.ceil(days));
