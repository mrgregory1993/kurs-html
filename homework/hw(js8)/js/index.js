"use strict";
// ## Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// ## Завдання

// Код для завдань лежить в папці project.

// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>

// 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

// #### Література:

// - [Пошук DOM елементів](https://uk.javascript.info/searching-elements-dom)

//////////// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let tagP = document.getElementsByTagName("p");
tagP = document.querySelectorAll("p");
// console.log(tagP);
tagP.forEach((val) => {
  val.style.cssText = "color: #ff0000";
});

///////////// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let optionsList = document.getElementById("optionsList");
// console.log(optionsList);
// console.log(optionsList.parentElement);
// console.log(optionsList.hasChildNodes());
// console.log(optionsList.childNodes);
// console.log(optionsList.children[1].nodeName);
// console.log(optionsList.children[1].nodeType);

////////////// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>
let testParagraph = document.getElementById("testParagraph");
// console.log(testParagraph);
testParagraph.innerHTML = "<p>This is a paragraph<p/>";
// console.log(testParagraph);

///////////// 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
let header = document.querySelector(".main-header");
header = [...header.getElementsByTagName("li")];
console.log(header);
header.forEach((el) => {
  // el.classList.add("nav-item")
//   el.classList += "nav-item";
  el.classList = "nav-item";

});
console.log(header);
// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
// let sectionProducts = document.querySelectorAll(".products-list-item");
// sectionProducts.forEach(el=>{
//     el.classList.remove("products-list-item")
//     console.log(el.classList);
// })
// sectionProducts.classList.remove("products-list-item")
