"use strict";
const root = document.getElementById("root");
let par = document.createElement("p");

par.textContent = "some text";
par.style.cssText = "font-size:20px; font-style:italic;";
par.className = "text-block";
console.log(par);

// //Поместить элемент в DOM
// root.append(par)///добавляет элемент в конец контетна
root.prepend(par); ///добавляет элемент в начало контетна

// root.after(par)///добавляет элемент после текущего элемента (<div id="root">root</div>)
// root.before(par)///добавляет элемент до текущего элемента (<div id="root">root</div>)
// root.after("<span>text</span>")///добавляет элемент после текущего элемента (<div id="root">root</div>)
// // root.remove()

//другие методы

root.insertBefore()
// root.insertAdjacentElement("afterbegin", par)// вставляет только обьекты ("куда",что вставить)
// root.insertAdjacentElement("afterbegin", "<span>text</span>")//не работает так как не обьект
// root.insertAdjacentHTML("afterbegin", "<span>text</span>")
// root.insertAdjacentHTML("afterbegin", par)
// par.insertAdjacentHTML("afterbegin", "<p>YES</p>")

// root.insertAdjacentText("beforebegin", "<span>text</span>")
// root.insertAdjacentText("beforebegin", par)

///Оператор три точки
// root.append(...[par,"text1", "text2"])
// root.append(par,"text1", "text2")

///Вставить одно и тоже в разные места дома КЛОНЫ
// const par2 = par.cloneNode(true)///аргумент обязателен (false/true)
// root.prepend(par2)

const docFragment = document.createDocumentFragment(); ///создает фрагмент документа
console.log(docFragment);

for (let i = 0; i < 20; i++) {
  const par2 = par.cloneNode(true);
  // root.prepend(par2)
  docFragment.append(par2);
}
console.log(docFragment);
root.prepend(docFragment);
console.log(docFragment);
