"use strict";
const arr = [1, 2, 3, "asd", true, function () {}];
// console.log(arr);
// console.log(typeof arr);
// console.log(arr[3]);
// console.log(arr.length);

// arr[0]=33
// delete arr[5]
// console.log(arr);

// for (const key in arr) {
//     console.log(arr[key]);
//     }

//     for (const iterator of arr) {
//         console.log(iterator);
//     }
// /////////////////////////////МЕТОДЫ ИЗМЕНЕНИЯ МАССИВА
// ///////////////////////////// метод push добавляет єлемент в конец массива
// arr.push("to end")
// console.log(arr);

// //////////////////////// метод pop УДАЛЯЕТ ПОСЛЕДНИЙ ЭЛЕМЕНТ С КОНЦА МАССИВА
// arr.pop("to end")
// console.log(arr);

// ///////////////////////////// метод УДАЛЯЕТ ПЕРВЫЙ ЭЛЕМЕНТ С МАССИВА
// arr.shift()
// console.log(arr);

// ////////////////////////// метод unshift добавляет в начало элемент в массив
// arr.unshift("to end")
// console.log(arr);

///////////////////////////// МЕТОДЫ КОПИРОВАНИЯ
// console.log(arr.slice(2,4));

// console.log(arr.splice(1, 0, "qweqwe", 3255)); ///-ВЫРЕЗАЕТ
// console.log(arr);

///////////////////////////// МЕТОД ПОИСКА В МАССИВЕ
// console.log(arr.includes(3)); //ищет элемент в масиве и возвращает true / false
// console.log(arr.indexOf(3,8)); // возвращают индекс одного значения или -1 если ненайден
// console.log(arr.lastIndexOf(3,3)); // возвращают индекс одного значения (начинает счет с конца)

// console.log(arr.join("-")); //обьединяет массива в строку
// console.log(arr);

// console.log(arr.reverse());

// console.log(arr.concat([[1, 2, 3, 4]], 7, 8, 9)); //обьединяет массивы и элементы
// console.log(arr);

///////////////////////////// МЕТОД forEach ПЕРЕБОР ЗНАЧЕНИЙ В МАССИВЕ
/////////////////////////////////////////////////VAR1
// arr.forEach(arrCbFn);
// function arrCbFn(Value, index, array) {
//   console.log(`${index} = ${Value} - ${array}`, array);
// }

/////////////////////////////////////////////////VAR2
// arr.forEach(function arrCbFn(elValue,index,array) {
//     console.log(`${index} = ${elValue} - ${array}`);
// })

/////////////////////////////////////////////////VAR2
// arr.forEach((elValue,index,array)=> {
//     console.log(`${index} = ${elValue} - ${array}`);
// })

//НУЖНО С ЭЛЕМЕНТОВ ОДНОГО МАСИВА СОЗДАТЬ ВТОРОЙ
const arr2 = [{ a: 3 }, { a: 4 }, { a: 5 }, { a: 6 }, { a: 36 }, { a: 23 }];
// console.log(arr2);
// console.log(arr2.map((el)=>el.a));//?????????????????????????

///////////////////////////// МЕТОД map СОЗДАЕТ НОВЫЙ МАССИВ
// let res;
// res = arr.map((value)=>{
//     return value+" loh"
// })
// console.log(res);

///////////////////////////// МЕТОД filter НЕ ОДНО ЗНАЧЕНИЕ А ВСЕ
// res = arr2.filter((elValue) => elValue >= 4);
// console.log(res);
//or
// console.log(arr2.filter((el) => el.a >= 4)); //el.a обращаемся к свойству обьекта( в массиве находятся обьекты)

///////////////////////////// МЕТОД find ТОЛЬКО ОДНО ЗНАЧЕНИЕ (ВОЗВРАЩАЕТ ПЕРВЫЙ ЭЛЕМЕНТ КОТОРЫЙ ПРОХОДИТ УСЛОВИЕ ВНУТРИ КАЛБЕК ФУНКЦИИ)
// res = arr.findIndex((elValue, index, array) => elValue >= 4);
// console.log(res);
//or
// console.log(arr2.find((el) => el.a > 4)); //el.a обращаемся к свойству обьекта( в массиве находятся обьекты)
// console.log(arr2.findIndex((el) => el.a > 4)); //el.a обращаемся к свойству обьекта( в массиве находятся обьекты)

///////////////////////////// МЕТОД every и some
// console.log(arr2.every((el) => el.a > 4)); // возвращает true в том случае если все элементы проходят проверку внутри КАЛБЕК ФУНКЦИИ
// console.log(arr2.some((el) => el.a > 4)); //возвращает true в том случае если хотя бы один элемент проходит проверку внутри КАЛБЕК ФУНКЦИИ

///////////////////////////// МЕТОД sort
// let arr2 = [1, 4, 3, 2, 6, 5, 8, 0];
// res = arr2.sort((a, b) => a - b);
// console.log(res);
//or
const arr3 = [1, 3, 5, 12, 4, 45, 7, 33, 222, 2];
// console.log(arr3.sort());//массив сортирует по битам
// console.log(arr3.sort((a,b)=>a-b));//массив сортирует по битам

// delete arr3[3]
// const compareFunction = (a,b)=>a-b
// console.log(arr3.sort(compareFunction));

///////////////////////////// МЕТОД reduce СУММА ЗНАЧЕНИЙ
// const arr = [1, 2, 3, 4, 5, 6];

// res = arr.reduce((startValue, currValue, currIndex, array) => {
//   return startValue * currValue;
// }, 1);
// console.log(res);
///////////////////////// МЕТОД reduceRight СУММА ЗНАЧЕНИЙ С ПОСЛЕДНЕГО ЭЛЕМЕНТА
// res = arr.reduceRight((startValue, currValue, currIndex, array) => {
//     return startValue * currValue;
//   }, 1);
//   console.log(res);

///////////////////////// МЕТОДЫ reduce и reduceRight НАХОДЯТ НАКОПИТЕЛЬНОЕ ЗНАЧЕНИЕ
console.log(arr3.reduce((res, value, index, array) => {
console.log(typeof res);
   return res + value;
},"{}"))

console.log(arr3.reduceRight((res, value, index, array) => {//накопление происходит с конца
    console.log(typeof res);
       return res + value;
    },{}))