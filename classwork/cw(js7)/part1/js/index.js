"use strict";

//////////////////////////////////////СТРОКИ///////////////////////////////////
const str = new String("Hello world!");
// console.log(typeof str);
// console .log(str);

const baseStr = "Hello world!";
// console.log(typeof baseStr);
// console .log(baseStr);
// console.log(baseStr);

// baseStr[4] = "5"
const baseStr2 = "Hello \n \u{1f60e} world!";
// console.log(baseStr2);

// console.log("za">"zb");
// console.log("az".charCodeAt(0));
// console.log("x".charCodeAt(0));

// console.log(baseStr.slice(0,6));
// console.log(baseStr.slice(6));

//////////////поиск подстроки
// console.log(baseStr.indexOf("ld",3));
// console.log(baseStr.lastIndexOf("ld",3));
// console.log(baseStr.includes("asd"));
// console.log(baseStr.endsWith("orld!"));
// console.log(baseStr.startsWith("He"));

// console.log(baseStr.toUpperCase());
// console.log(baseStr.toLocaleLowerCase());

// console.log(`    123    456   `);
// console.log(`    123    456   `.trim());

// console.log("abc ".repeat(2));
// console.log(baseStr.replace("o","opa"));
// console.log(baseStr.replaceAll("lo","opa"));

// for (let i = 0; i < baseStr.length; i++) {
//   // console.log(baseStr[i]);
// }
// for (const iterator of baseStr) {
//   console.log(iterator);
// }
// for (const key in baseStr) {
//   console.log(key + " = " + baseStr[key]);
// }


// console.log("Uasya, Petro, Piter".split(", "));


//////////////////////////////////////ДАТА///////////////////////////////////

// const date = new Date()
const date = new Date("1995-5-19")
// console.log(date.getDay());
// console.log(date.getFullYear());
// console.log(date.setFullYear(1995));
// console.log(date.getFullYear());
// console.log(date);
// console.log(Date.now()-date);
// console.log((Date.now()-date)/1000/60/60/24/365.25);

