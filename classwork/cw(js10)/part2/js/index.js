"use strict";
// /\*\*

// -   Задание 1.
// -
// -   Получить и вывести в консоль следующие элементы страницы:
// -   -   По идентификатору (id): элемент с идентификатором list;
// -   -   По классу — элементы с классом list-item;
// -   -   По тэгу — элементы с тэгом li;
// -   -   По CSS селектору (один элемент) — третий li из всего списка;
// -   -   По CSS селектору (много элементов) — все доступные элементы li.
// -
// -   Вывести в консоль и объяснить свойства элемента:
// -   -   innerText;
// -   -   innerHTML;
// -   -   outerHTML. \*/
////////////////РЕШЕНИЕ В КЛАССЕ
// console.log(document.getElementById("list"));
// console.log(document.getElementsByClassName("list-item"));
// console.log(document.getElementsByTagName("li"));
// console.log(document.querySelector("li:nth-child(3)"));
// console.log(document.querySelectorAll("li")[2]);
// console.log(innerText(ul));
// console.log(document.querySelector("li:nth-child(0n+1)").innerText);
// console.log(document.querySelector("ul"));
// console.log(document.querySelector("ul").innerText);
// console.log(document.querySelector("ul").textContent);
// console.log(document.querySelector("ul").innerHTML);
// console.log(document.querySelector("ul").outerHTML);

// document.querySelector("li:nth-child(3)").innerText+=`<b>Done</b>`;
// document.querySelector("li:nth-child(3)").textContent+=`<b>Done</b>`;
// document.querySelector("li:nth-child(3)").innerHTML+=` <b>Done</b>`;
///////////////////////////////////////////////////////

// /\*\*

// -   Задание 2.
// -
// -   Получить элемент с классом .remove.
// -   Удалить его из разметки.
// -
// -   Получить элемент с классом .bigger.
// -   Заменить ему CSS-класс .bigger на CSS-класс .active.
// -
// -   Условия:
// -   -   Вторую часть задания решить в двух вариантах: в одну строку и в две
//         строки.

// /\*

//   <ul class="list">
//   <li class="list-item remove">Item 1</li>
//   <li class="list-item">Item 2</li>
//   <li class="list-item">Item 3</li>
//   <li class="list-item">Item 4</li>
//   <li class="list-item bigger">Item 5</li>
//   <li class="list-item">Item 6</li>
//   <li class="list-item">Item 7</li>
//   </ul>

// \*/
////////////////РЕШЕНИЕ В КЛАССЕ
// let remove = document.querySelector(".remove");
// // remove= document.querySelector(".list1");
// remove.remove();
// console.log(remove);

// let bigger = document.querySelector(".bigger")
// // bigger.classList.remove("bigger")
// // bigger.classList.add("active")
// // bigger.className = bigger.className.replace("bigger","active")
// bigger.classList.replace("bigger","active")
// console.log(bigger.classList);
///////////////////////////////////////////////////////

// -   Задание 3.
// -
// -   На экране указан список товаров с указанием названия и количества на складе.
// -
// -   Найти товары, которые закончились и:
// -   -   Изменить 0 на «закончился»;
// -   -   Изменить цвет текста на красный;
// -   -   Изменить жирность текста на 600.
// -
// -   Требования:
// -   -   Цвет элемента изменить посредством модификации атрибута style.

// \*

// <ul class="store">
//     <li>Сыр: 05</li>
//     <li>Вода: 16</li>
//     <li>Кофе: 0</li>
//     <li>Масло: 102</li>
//     <li>Чай: 13</li>
//     <li>Молоко: 0</li>
//     <li>Мёд: 10</li>
// </ul>

// \*/

////////////////РЕШЕНИЕ МОЕ
// document.querySelector(".list").remove()
// document.querySelector(".list1").remove()
// console.log(document.querySelector(".store").innerText)
// console.log(document.querySelector(".store").innerText.indexOf(0));

// console.log(document.querySelector(".store").classList.forEach(value=>value===0));
// let arr = document.querySelector(".store").innerText;

///////////////////////////////////////////////////////

////////////////РЕШЕНИЕ В КЛАССЕ
document.querySelector(".list").remove();
document.querySelector(".list1").remove();

let arr = document.querySelectorAll(`.store > li`);
arr = [...arr];
arr.forEach((value, i, array) => {
  let productCount = value.textContent.split(":")[1].trim();
  console.log(productCount);
  if (productCount === "0") {
    value.textContent = value.textContent.replace("0", "закончился");
    // value.textContent = value.textContent.replace(productCount, "закончился");//мой вариант
    // value.style.color = "red"
    // value.style.fontWeight = 700
    value.style.cssText = "color: red; font-weight: 700";
  }
});

///////////////////////////////////////////////////////

// /\*\*

// -   Задание 4.
// -
// -   Получить элемент с классом .list-item.
// -   Отобрать элемент с контентом: «Item 5».
// -
// -   Заменить текстовое содержимое этого элемента на ссылку, указанную в секции
//     «дано».
// -
// -   Сделать это так, чтобы новый элемент в разметке не был создан.
// -
// -   Затем отобрать элемент с контентом: «Item 6».
// -   Заменить содержимое этого элемента на такую-же ссылку.
// -
// -   Сделать это так, чтобы в разметке был создан новый элемент.
// -
// -   Условия:
// -   -   Обязательно использовать метод для перебора;
// -   -   Объяснить разницу между типом коллекций: Array и NodeList. \*/

// /_ Дано _/ const targetElement =
// '<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google
// it!</a>';

// /\*

//     <ul class="list">
//     <li class="list-item">Item 1</li>
//     <li class="list-item">Item 2</li>
//     <li class="list-item">Item 3</li>
//     <li class="list-item">Item 4</li>
//     <li class="list-item">Item 5</li>
//     <li class="list-item">Item 6</li>
//     <li class="list-item">Item 7</li>
//     </ul>

// \*/
