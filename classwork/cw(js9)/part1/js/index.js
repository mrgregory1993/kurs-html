"use strict";
// ## TASK 1

// Напишите функцию mergeArrays для объединения нескольких массивов в один.

// Функция обладает неограниченным количеством параметров. Функция возвращает один
// массив, который является сборным из массивов, переданных функции в качестве
// аргументов при её вызове.

// Условия:

// -   Все аргументы функции должны обладать типом «массив», иначе генерировать
//     ошибку;
// -   В ошибке обязательно указать какой по счёту аргумент провоцирует ошибку.
//     Заметки:
// -   Делать поддержку выравнивания вложенных массивов (флеттенинг) не нужно.

// function mergeArrays(...args) {
//   let index = args.findIndex((value) => !Array.isArray(value));
//   if (index !== -1) {
//     console.log(`Error is ${++index}`);
//   } else {
//     let newArr = [];
//     args.forEach((value) => (newArr = newArr.concat(value)));
//     return newArr;
//   }
// }
// console.log(mergeArrays([1, 2, 0, 4, 3, 6], [12], ["asda"]));

// ## TASK 2

// Написать функцию-исследователь навыков разработчика developerSkillInspector.
// Функция не обладает аргументами и не возвращает значение. Функция опрашивает
// пользователя до тех пор, пока не введёт хотя-бы один свой навык. Если
// пользователь кликает по кнопке «Отменить» в диалоговом окне, программа
// оканчивает опрос и выводит введённые пользователем навыки в консоль. Условия:

// -   Список навыков хранить в форме массива обьектов;
// -   Каждый обьект массива должен иметь следующую структуру

//         {
//             id: value,
//             skillsName: value,
//             skillsLevel: value,
//         }

// id - числовое значение, у первого обьекта значение должно быть ранво 0. У
// остальных обьектов значение увеличивается на 1. skillsName - название навыка
// которое ввел пользователь (строки длиной больше, чем один символ). skillsLevel -
// уровень навыка, один из трех значений: junior, middle, senior. Значение выбирает
// пользователь.

// function developerSkillInspector() {
//   let skills = []
//   let skill = {}
//   do{
//   let sName = prompt("Ваш навык","asdasd");
//   }
//   while (sName !== "" || sName !== null || sName.length >= 1) {
//     console.log("No");
//   }

//   // if (sName === "" || sName === null || sName.length <= 1) {
//   //   console.log("No");
//   // }
//   let sLevel = prompt("Уровень навыка");
// }
// developerSkillInspector();

////////////////////РЕШЕНИЕ 2
function developerSkillInspector() {
  let skills = [];
  let sName;
  let sLevel;
  do {
    sName = prompt("Ваш навык");
    sLevel = prompt("Уровень навыка (junior, middle, senior)");
    while ((sName === null && skills.length === 0) || sName.length < 2) {
      sName = prompt("Ваш навык");
    }
    while (
      (sLevel === null && skills.length === 0) ||
      (sLevel !== "junior" && sLevel !== "middle" && sLevel !== "senior")
    ) {
      sLevel = prompt("Уровень навыка");
    }
    let skill = {
      id: skills.length,
      skillsName: sName,
      skillsLevel: sLevel,
    };
    skills.push(skill);
  } while (sName !== null && sLevel !== null);
  console.log(skills);
}
developerSkillInspector();

//////////////////////////////

// ## TASK 3

// Напишите функцию, которая принимает в параметры 2 объекта - резюме и вакансию, и
// возвращает процент совпадения требуемых навыков (скиллов). Навык совпадает если
// имя скилла совпадает с имененм в вакансии и и требуемый опыт <= опыту человека в
// этом навыке

//     const resume = {
//         name: "Илья",
//         lastName: "Куликов",
//         age: 29,
//         city: "Киев",
//         skills: [
//             { name: "Vanilla JS", practice: 5 },
//             { name: "ES6", practice: 3 },
//             { name: "React + Redux", practice: 1 },
//             { name: "HTML4", practice: 6 },
//             { name: "CSS2", practice: 6 }
//             ]
//         };

//     const vacancy = {
//         company: "SoftServe",
//         location: "Киев",
//         skills: [
//             { name: "Vanilla JS", experience: 3 },
//             { name: "ES6", experience: 2 },
//             { name: "React + Redux", experience: 2 },
//             { name: "HTML4", experience: 2 },
//             { name: "CSS2", experience: 2 },
//             { name: "HTML5", experience: 2 },
//             { name: "CSS3", experience: 2 },
//             { name: "AJAX", experience: 2 },
//             { name: "Webpack", experience: 2 }
//             ]
//         };

// ## TASK 4

// Напишите функцию, которая принимает в себя информацию об пользователе в виде
// объекта, среди параметров которого - поля age и wantLicense, и которая
// возвращает ответ - может ли человек получить в его возрасте желаемые права.
// Функция содержит в себе массив объектов, описывающий разные типы водительских
// прав.

//     const user = {
//         age: 21,
//         name: "Алексей",
//         lastName: "Михайлович",
//         wantLicense: "B1"
//         };

//     const driverLicence = [
//         { age: 16, type: "A1", time: 2 },
//         { age: 16, type: "A2", time: 2 },
//         { age: 18, type: "B1", time: 6 },
//         { age: 18, type: "B",  time: 4 },
//         { age: 19, type: "BE", time: 4 },
//         { age: 21, type: "D1", time: 3 }
//         ];
