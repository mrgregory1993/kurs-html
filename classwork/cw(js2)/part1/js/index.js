"use strict";

// !, &&, ||

// let a = "text"
/// console.log(!1);

// console.log(10 && null);
// console.log("a" && null);

// console.log(10 && "a");
// console.log(!10 && "a"); // false?
// console.log(true && "TT");
// console.log(null && 0);
// console.log(1 && NaN);
// console.log(1 && !0);  // true?
// console.log(10 && null && 0); 
// console.log(NaN && null && 0); 



// console.log(!10 || "a");

// console.log((!10 && null) || undefined);
// console.log(0 || 10 || undefined || (true && "TT"));
// console.log(10 || undefined || (true && "TT"));
// console.log(10 || (true && "TT"));
// console.log(10 || "TT");
// console.log(10);
//------------------------//
// console.log(false || "TT");
// console.log(0 || "TT");
// console.log(true || "TT");
// console.log(10 || true);
//------------------------//

// console.log(0 || 10 === 10);
// console.log(0 || 10);
// console.log(0 || 10 > 5);
// console.log(0 || 10 / 0); //Infinity?
// console.log(0 || 10 > 10);
// console.log(0 || 10 >= 10);

// let num = 1;
// console.log(isNaN(num));
// let num = undefined;
// console.log(isNaN(num));
// let num = NaN;
// console.log(isNaN(num));
// let num = null;
// console.log(isNaN(num));

// let num = "null";
// console.log(Number.isNaN(num));

// let num = !"a";
// console.log(Number.isNaN(num));
// let num = NaN;
// console.log(Number.isNaN(num));

// if (condition) {
//     console.log("Done");
// }
// if (true) {
//     console.log("Done");
// }

// if (0) {
//     console.log("Done");
// }
// else if (!10) {
//     console.log("else here");
// }
// else {
//     console.log("0");
// }

// let num2 = 12;
// if (num2 === 0) {
//     console.log("Done");
// }
// else if (num2 === 10) {
//     console.log("10");
// }
// else if (num2 !== 12) {
//     console.log("else here");
// }
// else {
//     console.log("0");
// }


// let choise = "10";
// switch (choise) {
//     case "A":
//         console.log("A");
//         break;
//     case "10":
//         console.log("a");
//         break
//     default: 
//         console.log("default");
// }


// let logged = true;
// let status = logged ? "user logged in" : "please log in"

// const MAX_TIMES = 100;
// for (let i = 0; i < MAX_TIMES; i++) {
//     console.log(i);
// }




//------------------------//

// for (let index = 0; index < 500; index++) {
//     console.log(index);
//     if (index > 455) {
//         continue;
//     }
//     console.log("end loop");
// }

//------------------------//


// let a = 0;
// while (a < 3) {
//     a++;
//     if (a === 1){
//         // break;
//     continue;
//     }
//     console.log(a);
// }


// let a = 0;
// do {
//     console.log("Do done");
// }
// while (a < 0);



// let a = 3;
// do {
//     a = a + 0.5;
// }
// while (a < 3);
// console.log(a);