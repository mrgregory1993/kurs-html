"use strict";

let flower = "romashka";
let plant = "wood";

const user = {
  // name:"Loh",
  userStatus: "Govno",
  "nick name": "Uasya",
  _age: 13,

  // setAge(value) {
  //   if (value > 0) {
  //     this.age = value;
  //   }
  // },

  set age(value) {
    if (value > 0) {
      this._age = value;
    }
    // console.log("setter");
    // this._age = value;
  },
  get age() {
    // console.log("getter");
    return this._age + " years";
  },
  // [flower]: flower, //!!!!!!!!!!!!!!ДОБАВЛЕНИЕ СВОЙСТВА С ПЕРЕМЕННОЙ let
  // [plant]: flower, //!!!!!!!!!!!!!!ДОБАВЛЕНИЕ СВОЙСТВА С ПЕРЕМЕННОЙ let
  // plant: flower, //!!!!!!!!!!!!!!ДОБАВЛЕНИЕ СВОЙСТВА С ПЕРЕМЕННОЙ let

  // family: {
  //   //////////вложение обьекты/////////
  //   count: 2,
  //   lovely: "Yes",
  //   "nick name": "Baba",
  // },
  // showAge: function () {
  //   //// метод обьекта var1////////////
  //   return this.age;
  // },

  // showStatus() {
  //   //// метод обьекта var2////////////
  //   return this.userStatus;
  // },
};


// console.log(flower in user);
// console.log(Object.hasOwn(user,"name"));
// console.log(Object.keys(user));
// console.log(user);

// for (const key in user) {
//   console.log(key);
//   }

// user.setAge(22);
user.age = -1;
console.log(user.age);
// console.log(user.age);
// console.log(user["age"]);
// console.log(user["nick name"]);
// console.log(user.family["nick name"]);
// user.id = 0;
// delete user.romashka;
// console.log(user.showStatus());
// console.log(user);

/////////////////////////////Добавление обьектов var1
// const user2 = user;

// user2["nick name"] = "Uasilisya";

// console.log(user);
// console.log(user2);

////////////////////////////Добавление обьектов var2
// const user2 = {};

// for (const key in user) {
//   user2[key] = user[key];
// //   console.log(key);
// }
// user2["nick name"] = "Uasilisya";
// console.log(user);
// console.log(user2);

////////////////////////////Добавление копии обьектов var3

// const user3 = Object.assign({},user)
// user3["nick name"] = "Vika";
// console.log(user);
// console.log(user3);
// console.log(typeof{} instanceof Object);
// console.log(typeof null instanceof Object)
// console.log(typeof[] instanceof Object);

// console.log({} instanceof Object);
// console.log(null instanceof Object)
// console.log([] instanceof Object);
// console.log([] instanceof Array);

// const user2 = {};

// for (const key in user) {
//   user2[key] = user[key];
// console.log(key);
// }
// user2["nick name"] = "Uasilisya";
// console.log(user);
// console.log(user2);
// user2.userStatus = "Ne govno"
// console.log(user2.showStatus());
