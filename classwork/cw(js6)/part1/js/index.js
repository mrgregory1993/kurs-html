"use strict";
// ## TASK 1

// Создать функцию, которая проверяет пустой ли обьект (есть ли хоть какие-то поля
// внутри обьекта ). Функция должна возвращать true если поля есть, иначе false.

//////////////////////////////////////РЕШЕНИЕ///////

// const obj = {
//     name:"Kukan"
// }
// const obj2 = {
// }

// function name(obj) {
//   for (const key in obj) {
//     return true;
//   }
//   return false;
// }
// console.log(name(obj));
//////////////////////////////////////////////////////////

// ## TASK 2

// Напишите функцию, которая принимает аргументом обьект и проверяет сколько в нем полей с примитивными значениями, а сколько полей со структурными значениями.

//////////////////////////////////////РЕШЕНИЕ ДОДЕЛАТЬ///////

// const obj1 = {
//     name:"Kukan"
// }
// const obj2 = {
//     name:"Kukan",
//     param:{
//         length: 180,
//         hight: 250
//     }

// }

// const obj3 = {
//     name:"Kukan",
//     param: ["length: 180", "hight: 250"]
// }

// function name(obj) {

// }
// console.log(obj3);
// console.log(obj2);

//////////////////РЕШЕНИЕ НА УРОКЕ////////////////////
//   const countObjectTypes = (obj) => {
//     let premitiveTypesCounter = 0;
//     let structerTypesCounter = 0;
//     const valuesOfobj = Object.values(obj);
//     for(let i =0;i<valuesOfobj.length;i++){
//       if (typeof valuesOfobj[i] === "number" || typeof valuesOfobj[i] === "string" || typeof valuesOfobj[i] === "undefined" || typeof valuesOfobj[i] === "null" || typeof valuesOfobj[i] === "boolean"){
//         premitiveTypesCounter++;
//       } else {
//         structerTypesCounter ++;
//       }

//     }
//     console.log(valuesOfobj)
//     console.log(`Примитивов: ${premitiveTypesCounter} Структурных: ${structerTypesCounter}`);
//   }
//   countObjectTypes(objTest);

//////////////////////////////////////////////////////////

// ## TASK 3

// Создайте объект city1 (var city1 = {}), укажите у него свойства name (название
// города, строка) со значением «ГородN» и population (населенность города, число)
// со значением 10 млн.

//////////////////////////////////////РЕШЕНИЕ///////
// const city1 = {
//   name: "ГородN",
//   population: "10 млн",
// };
// console.log(city1);
//////////////////////////////////////////////////////////

// ## TASK 4

// Дополните предыдущую задачу. Создайте объект city2 через нотацию {name:
// "ГородM", population: 1e6}.
//////////////////////////////////////РЕШЕНИЕ///////
// const city1 = {
//   name: "ГородN",
//   population: "10 млн",
// };
// const city2 = {
//   name: "ГородM",
//   population: 1e6,
// };
// console.log(city1, city2);

//////////////////////////////////////////////////////////

// ## TASK 5

// Дополните предыдущую задачу. Создайте у объектов city1 и city2 методы getName(),
// которые вернут соответствующие названия городов
//////////////////////////////////////РЕШЕНИЕ///////
// const city1 = {
//   name: "ГородN",
//   population: "10 млн",
//   getName() {
//     return this.name;
//   },
// };
// const city2 = {
//   name: "ГородM",
//   population: 1e6,
//   getName() {
//     return this.name;
//   },
// };
// console.log(city1.getName(), city2.getName());

//////////////////////////////////////////////////////////

// ## TASK 6

// Дополните предыдущую задачу. Создайте методы exportStr() у каждого из объектов.
// Этот метод должен возвращать информацию о городе в формате
// «name=ГородN\npopulation=10000000\n». Для второго города будет строка со своими
// значениями. Примечание: можно обращаться к каждому свойству через цикл for/in,
// но методы объекта возвращать не нужно

//////////////////////////////////////РЕШЕНИЕ///////
// const city1 = {
//   name: "ГородN",
//   population: 10e6,
//   getName() {
//     return this.name;
//   },
//   exportStr(){
//     return(`name = ${this.name}/${this.population}`);
//   }
// };
// const city2 = {
//   name: "ГородM",
//   population: 1e6,
//   getName() {
//     return this.name;
//   },
//   exportStr(){
//     return(`${this.name}/${this.population}`);
// }

// };
// console.log(city1.exportStr());
// console.log(city2.exportStr());

//////////////////////////////////////////////////////////

// ## TASK 7

// Создайте базу данных заработных плат сотрудников в виде одного обьекта {name1:
// sallary1, name2: sallary2, name3: sallary3, ...}. Данные запрашиваются у
// пользователя. В качестве названия поля используются имена сотрудников, значение
// поля - заработная плата.

//////////////////////////////////////РЕШЕНИЕ///////

// const zp = {
//   newUser(name, sallary) {
//     name = prompt("Your name");
//     sallary = prompt("Your sallary");
//     this[name] = sallary;
//   },
// };
// zp.newUser();
// console.log(zp);

//////////////////////////////////////////////////////////

// ## TASK 8

// Создайте глобальную функцию getObj(), которая возвращает this. А у каждого из
// объектов city1 или city2 метод getCity, который ссылается на getObj. Проверьте
// работу метода. Примечание: к объекту вызова можно обратиться через this.

//////////////////////////////////////РЕШЕНИЕ///////
// function getObj() {
//     return this;
// };
// const city1 = {
//   name: "ГородN",
//   population: 10e6,
//   getCity: getObj////////////////ССЫЛАТЬСЯ НА ФУНКЦИЮ getObj()///
// };
// const city2 = {
//   name: "ГородM",
//   population: 1e6,
//   getCity: getObj////////////////ССЫЛАТЬСЯ НА ФУНКЦИЮ getObj()///
// };
// console.log(city1.getCity());
// console.log(city2.getCity());

// console.log(city2.exportStr());
//////////////////////////////////////////////////////////

// ## TASK 9

// Создать объект obj, с методами method1(),method2() и method3(). В методе
// method3() должна возвращаться строка «метод3». Сделайте так, чтобы было возможно
// выполнение кода obj.method1().method2().method3().

//////////////////////////////////////РЕШЕНИЕ///////
// const obj = {
//   method1() {
//     return this;
//     // return this.method2();
//   },
//   method2() {
//     return this;
//     // return this.method3();
//   },
//   method3() {
//     return "метод3";
//   },
// };
// console.log(obj.method1().method2().method3());
// console.log(obj.method1());

//////////////////////////////////////////////////////////

// ## TASK 10

// Напишите функцию createProduct, которая будет создавать объекты, описывающие
// товары. У товара должны быть такие свойства:

// -   name;
// -   fullName;
// -   article;
// -   price. При этом при попытке напрямую (через точку) изменить свойство price
//     происходит его его проверка на прравильность: цена должна быть целым
//     положительным числом. Если эти требования нарушаются - присвоения не
//     произойдет. Создавать его аналог через "\_price" нельзя.

// Пример работы: const notebook = createProduct("lenovo X120S", "lenovo X120S
// (432-44) W", 3332, 23244); console.log(notebook.price);// выведет 23244
// notebook.price = -4; // присвоение не произойдет console.log(notebook.price);//
// выведет 23244 notebook.price = 22000; console.log(notebook.price);// выведет
// 22000

//////////////////////////////////////РЕШЕНИЕ///////
function createProduct(name, fullName, article, price) {
  const product = {
    name,
    fullName,
    article,
    price,

    // set newPrice(value) {
    //   if (value > 0) {
    //     this.price = value;
    //   }
    // },
    // get age() {
    //   return;
    // }
  };
  Object.defineProperty(product, "price", {
    set: (value) {
      if (value>0) {
        price = value;
      }
    },
    // get: () {
    //   return price;
    // },
    // writable: false,
    // writable: true,
    // value: 35000,
    // configurable: false,
    // configurable: true,
  
  });
  return product;
}
// function createProduct()
let notebook = createProduct(
  "lenovo X120S",
  "lenovo X120S (432-44) W",
  3332,
  23244
);
// console.log(notebook);

console.log(notebook.price); //выведет 23244
notebook.price = -4; // присвоение не произойдет
console.log(notebook.price); // выведет 23244
//  notebook.price = 22000;
//  console.log(notebook.price);// выведет 22000
/////////////////////////////////////////////////////////
