"use strict";
///список гостей, состоящий из нескольких строк
// let guestList = `Guests:
//  * John
//  * Pete 
//  * Mary`;
// alert(guestList);

///список гостей, состоящий из нескольких строк но не работает с кавычками ""
// let guestList = "Guests:
//  * John
//  * Pete 
//  * Mary";

// alert(guestList);

///список гостей, состоящий из нескольких строк («символ перевода строки»,который записывается как \n)
// let guestList = "Guests:\n * John\n * Pete\n * Mary";
// alert(guestList);

//////////////Длинные юникодные коды
// 佫, редкий китайский иероглиф:
// alert( "\u{20331}" );
// console.log("\u{20331}");
// 😍, лицо с улыбкой и глазами в форме сердец:
// alert( "\u{1F60D}" );
// console.log("\u{1F60D}")

// Все спецсимволы начинаются с обратного слеша, \ — так называемого «символа экранирования».

// Он также используется, если необходимо вставить в строку кавычку.

// К примеру:
// \'
// alert( 'I\'m the Wa\'lrus!' ); // I'm the Walrus!


/////////////////////////Доступ к символам
// let str = `Hello`;

// получаем первый символ
// alert( str[0] ); // H
// alert( str.charAt(0) ); // H

// получаем последний символ
// alert( str[str.length - 1] ); // o


///////////////////////Также можно перебрать строку посимвольно, используя for..of:

// let str = `Hello`;
// for (let char of str) {
//   alert(char); // H,e,l,l,o (char — сначала "H", потом "e", потом "l" и т. д.)
//   console.log(char); // H,e,l,l,o (char — сначала "H", потом "e", потом "l" и т. д.)
// }

/////////////////////////////////////////Изменение регистра
// // Методы toLowerCase() и toUpperCase() меняют регистр символов:

// alert( 'Interface'.toUpperCase() ); // INTERFACE
// alert( 'Interface'.toLowerCase() ); // interface
// // Если мы захотим перевести в нижний регистр какой-то конкретный символ:

// alert( 'Interface'[0].toLowerCase() ); // 'i'


/////////////////////////////////////////////Получение подстроки

// let str = "stringify";

// // начинаем с позиции 4 справа, а заканчиваем на позиции 1 справа
// console.log(str.slice(-4, -1)); // gif
// console.log(str.slice(5, 8)); // gif





////////////////////////Возвращает код для символа, находящегося на позиции pos:

// одна и та же буква в нижнем и верхнем регистре
// будет иметь разные коды
// console.log("z".codePointAt(0));
// console.log("A".codePointAt(0));
// console.log("0".codePointAt(0));
// console.log("9".codePointAt(0));

// ///////////////////////////Создаёт символ по его коду code

// console.log(String.fromCodePoint(30));

//////////////////////////////////Давайте сделаем строку, содержащую символы с кодами от 65 до 220 — это латиница и ещё некоторые распространённые символы:

// let str = '';

// for (let i = 65; i <= 220; i++) {
//   str += String.fromCodePoint(i);
// }
// console.log( str );


// Итого
// Есть три типа кавычек. Строки, использующие обратные кавычки, могут занимать более одной строки в коде и включать выражения ${...}.
// Строки в JavaScript кодируются в UTF-16.
// Есть специальные символы, такие как \n, и можно добавить символ по его юникодному коду, используя \u....
// Для получения символа используйте [].
// Для получения подстроки используйте slice или substring.
// Для того, чтобы перевести строку в нижний или верхний регистр, используйте toLowerCase/toUpperCase.
// Для поиска подстроки используйте indexOf или includes/startsWith/endsWith, когда надо только проверить, есть ли вхождение.
// Чтобы сравнить строки с учётом правил языка, используйте localeCompare.
// Строки также имеют ещё кое-какие полезные методы:

// str.trim() — убирает пробелы в начале и конце строки.
// str.repeat(n) — повторяет строку n раз.
// ...и другие.



///////////////////////////////////////Дата и время////////////////////////////////

// let now = new Date();
// console.log( now ); // показывает текущие дату и время


// // 0 соответствует 01.01.1970 UTC+0
// let Jan01_1970 = new Date(-10000000000000);
// console.log( Jan01_1970 );

// // теперь добавим 24 часа и получим 02.01.1970 UTC+0
// let Jan02_1970 = new Date(24 * 3600 * 1000);
// console.log( Jan02_1970 );


// // текущая дата
// let date = new Date();
// console.log(date);


// // час в вашем текущем часовом поясе
// console.log( date.getHours() );

// // час в часовом поясе UTC+0 (лондонское время без перехода на летнее время)
// console.log( date.getUTCHours() );


// let date1 = +prompt("asdad") 
// let date = new Date(date1);
// console.log(date);

// let date = new Date(2013, 0, 32); // 32 Jan 2013 ?!?
// console.log(date); // ...1st Feb 2013!



// function diffSubtract(date1, date2) {
//     return date2 - date1;
//   }
  
//   function diffGetTime(date1, date2) {
//     return date2.getTime() - date1.getTime();
//   }
  
//   function bench(f) {
//     let date1 = new Date(0);
//     let date2 = new Date();
  
//     let start = Date.now();
//     for (let i = 0; i < 100000; i++) f(date1, date2);
//     return Date.now() - start;
//   }
  
//   alert( 'Время diffSubtract: ' + bench(diffSubtract) + 'мс' );
//   alert( 'Время diffGetTime: ' + bench(diffGetTime) + 'мс' );
// Вывести дату в формате дд.мм.гг
// Напишите функцию formatDate(date), которая выводит дату date в формате дд.мм.гг:
// Например:
// var d = new Date(2014, 0, 30); // 30 января 2014
// alert( formatDate(d) ); // '30.01.14'
// P.S. Обратите внимание, ведущие нули должны присутствовать, то есть 1 января 2001 должно быть 01.01.01, а не 1.1.1.

// function formatDate(date) {

//     var dd = date.getDate();
//     if (dd < 10) dd = '0' + dd;
  
//     var mm = date.getMonth() + 1;
//     if (mm < 10) mm = '0' + mm;
  
//     var yy = date.getFullYear() % 100;
//     if (yy < 10) yy = '0' + yy;
  
//     return dd + '.' + mm + '.' + yy;
//   }
  
//   var d = new Date(2014, 0, 30); // 30 Янв 2014
//   alert( formatDate(d) ); // '30.01.14'