"use strict";
///DOM-узел можно создать двумя методами:
// document.createElement(tag)//Создаёт новый элемент с заданным тегом:
// let div = document.createElement('div');

// document.createTextNode(text)//Создаёт новый текстовый узел с заданным текстом:
// let textNode = document.createTextNode('А вот и я');
let div = document.createElement("div");
div.className = "alert";
div.innerHTML = "<strong>Всем привет!</strong> Вы прочитали важное сообщение.";
////////////////////////Методы вставки
///Чтобы наш div появился, нам нужно вставить его где-нибудь в document. Например, в document.body.
///Для этого есть метод append, в нашем случае:
document.body.append(div)
///Вот методы для различных вариантов вставки:
///node.append(...nodes or strings) – добавляет узлы или строки в конец node,
///node.prepend(...nodes or strings) – вставляет узлы или строки в начало node,
///node.before(...nodes or strings) –- вставляет узлы или строки до node,
///node.after(...nodes or strings)–- вставляет узлы или строки после node,
///node.replaceWith(...nodes or strings) –- заменяет node заданными узлами или строками.

ol.before('before'); // вставить строку "before" перед <ol>
ol.after('after'); // вставить строку "after" после <ol>

let liFirst = document.createElement('li');
liFirst.innerHTML = 'prepend';
ol.prepend(liFirst); // вставить liFirst в начало <ol>

let liLast = document.createElement('li');
liLast.innerHTML = 'append';
ol.append(liLast); // вставить liLast в конец <ol>