"use strict";
// function declaracion
// function summ() {
//   console.log(2 + 3);
// }
// summ()
// summ()
//
//
//
//

// function expression
// // summ2()
// let summ2 = function () {
//   console.log(3 + 3);
// };
// summ2();
// let result;
// function summ(a, b, c) {
//   //   console.log(a + b + c);
//   let summ = a + b + c;
//   return summ;
// }
//
//
//
//

// result = summ(10, 3, 2);
// console.log(result);
// result = summ(10, 10, 6);
// console.log(result);
// result = summ(10, 5, 1);
// console.log(result);
// result = summ(10, 9, 5);
// console.log(result);

//
//
//
//
// function argsFromOutside() {
//     for (let i = 0; i < arguments.length; i++) {
//         console.log(arguments[i]);

//     }
//   return arguments;
// }
// console.log(argsFromOutside(20, 2555, 33, 48, 65, 9996, 300007));
// console.log(argsFromOutside(1, 2, 3, 4, 5));
// console.log(argsFromOutside(1, 2, 3, 4, 5, 6, 7, 8, 9));

function argsFromOutside(a, b, c) {
  let summ = 0;
  
  for (let i = 0; i < arguments.length; i++) {
    summ = summ + arguments[i];
  }
  console.log(summ);
  return summ;
}
argsFromOutside(20, 2555, 33, 48, 65, 9996, 300007);
