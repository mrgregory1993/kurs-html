"use strict";
// /////////////////////////////МЕТОДЫ СОЗДАНИЯ МАССИВА

// /////////////////////////////1 метод ЛИТЕРАЛ
// const arr = [
//   1,
//   "asd",
//   null,
//   true,
//   function name(params) {},
//   [1, 2, 3],
//   { a: 1 },
// ];
// console.log(arr);
// console.log(typeof arr);
// arr.reverse()
// console.log(arr);
// console.log(arr.length);
// console.log(arr[5]);

// /////////////////////////////2 метод
// const str = "hello"
// const newArr = str.split("")///    "" - СЕПАРАТОР
// console.log(newArr);

// const newStr = newArr.join("-")
// console.log(newStr);

// /////////////////////////////МЕТОДЫ НАПОЛНЕНИЯ МАССИВА
// /////////////////////////////1 метод push
// arr.push("to end")
// console.log(arr);

// arr.push("to end", 11,"asdasd")
// console.log(arr);

// /////////////////////////////2 метод unshift
// arr.unshift("to end")
// console.log(arr);

// /////////////////////////////3 метод pop УДАЛЯЕТ ПОСЛЕДНИЙ ЭЛЕМЕНТ С МАССИВА
// arr.pop("to end")
// console.log(arr);

// /////////////////////////////4 метод УДАЛЯЕТ ПЕРВЫЙ ЭЛЕМЕНТ С МАССИВА
// arr.shift("to end")
// console.log(arr);

/////////////////////////////ПЕРЕБОР МАССИВА
// for (let i = 0; i < arr.length; i++) {
//     console.log(arr[i]);
// }

// for (const iterator of arr) {
//     console.log(iterator);
// }

///////////////////////////// ПУСТЫЕ ЯЧЕЙКИ В МАССИВЕ
/////////////////////////////

// const arr2 = [];
// console.log(arr2.length);
// arr2.length = 4;
// console.log(arr2);

// arr2.push(1, 2, 3, 4, 5);
// console.log(arr2);

// arr2[12] = 12;
// console.log(arr2);

// delete arr2[6]
// console.log(arr2);

// /////////////////////////////МЕТОДЫ МАССИВА
///////////////////////////// МЕТОД ПОИСКА В МАССИВЕ

// const arr = [
//   9,
//   "asd",
//   null,
//   4,
//   function name(params) {},
//   [1, 2, 3],
//   { a: 1 },
//   3,
//   88,
// ];
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

// console.log(arr);

// console.log(arr.indexOf(3));
// console.log(arr.indexOf(3,8));
// console.log(arr.lastIndexOf(3,3));
// console.log(arr.includes(3));
// arr.concat([1, 2, 3, 4], 7, 8, 9);
// console.log(arr.concat([1, 2, 3, 4], 7, 8, 9));
///////////////////////////// МЕТОД ПОЛУЧЕНЯ ЧАСТИ МАССИВА

// console.log(arr.slice(2,4));
// console.log(arr.splice(1,1));
// console.log(arr);
// console.log(arr.splice(1,0,"qweqwe",3));///-ДОБАВЛЯЕТ С 3 ЗНАЧЕНИЯ

///////////////////////////// МЕТОД callBack FUNCTION!!!!!!!!!!!!!!!!!!!!!!!!!!
// function callBackFn() { }
// function fnName(a,b, callBack) {
//     a++
//     a+b
//     callBack()
// }
// fnName(2,3,callBackFn)
// console.log(fnName(2,3,callBackFn));

///////////////////////////// МЕТОД forEach ПЕРЕБОР ЗНАЧЕНИЙ В МАССИВЕ
// function arrCbFn(element) {
//     console.log(element);
// }
// arr.forEach(arrCbFn)

/////////////////////////////////////////////////VAR1
// function arrCbFn(elValue,index,array) {
//     console.log(`${index} = ${elValue} - ${array}`);
// }
// arr.forEach(arrCbFn)

/////////////////////////////////////////////////VAR2
// arr.forEach(function arrCbFn(elValue,index,array) {
//     console.log(`${index} = ${elValue} - ${array}`);
// })

/////////////////////////////////////////////////VAR2
// arr.forEach((elValue,index,array)=> {
//     console.log(`${index} = ${elValue} - ${array}`);
// })
///////////////////////////// МЕТОД find
// let res = arr.find((elValue,index,array)=>{
// return elValue>4
// })
// console.log(res);
/////////////////////////////////////////////////VAR2
let res;
// res = arr.find((elValue, index, array) => elValue > 4);
// console.log(res);

///////////////////////////// МЕТОД find ТОЛЬКО ОДНО ЗНАЧЕНИЕ

// res = arr.findIndex((elValue, index, array) => elValue >= 4);
// console.log(res);

///////////////////////////// МЕТОД filter НЕ ОДНО ЗНАЧЕНИЕ
res = arr.filter((elValue)=>elValue>=4)
console.log(res);

///////////////////////////// МЕТОД map СОЗДАЕТ НОВЫЙ МАССИВ
// res = arr.map((value)=>{
//     return value+" loh"
// })
// console.log(res);

///////////////////////////// МЕТОД sort
// let arr2 = [1, 4, 3, 2, 6, 5, 8, 0];
// res = arr2.sort((a, b) => a - b);
// console.log(res);

///////////////////////////// МЕТОД reduce СУММА ЗНАЧЕНИЙ
// const arr = [1, 2, 3, 4, 5, 6];

// res = arr.reduce((startValue, currValue, currIndex, array) => {
//   return startValue * currValue;
// }, 1);
// console.log(res);

///////////////////////// МЕТОД reduceRight СУММА ЗНАЧЕНИЙ С ПОСЛЕДНЕГО ЭЛЕМЕНТА
// res = arr.reduceRight((startValue, currValue, currIndex, array) => {
//     return startValue * currValue;
//   }, 1);
//   console.log(res);

// let fruits = ["Apples", "Pear", "Orange"];

// // додаємо нове значення в "копію"
// let shoppingCart = fruits;
// shoppingCart.push("Banana");

// // Що в fruits?
// // alert( fruits.length ); // ?
// console.log(fruits);
// console.log(shoppingCart);

// function sumAll(...args) {
//   // args – це ім’я масиву
//   let sum = 0;
//   for (let key of args) sum += key;
//   return sum;
// }

// console.log(sumAll(1)); // 1
// console.log(sumAll(1, 2)); // 3
// console.log(sumAll(1, 2, 3, 12, 33, 43));

// function sumAll(...args) {
//     // args – це ім’я масиву
//     let sum = 0;
//     for (let key of args) sum += key;
//     return sum;
//   }
  
//   console.log(sumAll(1)); // 1
//   console.log(sumAll(1, 2)); // 3
//   console.log(sumAll(1, 2, 3, 12, 33, 43));
  
  // function showName(firstName, lastName, ...titles) {
  //   console.log( firstName + ' ' + lastName ); // Юлій Цезар
  
  //   // решта параметрів переходять до масиву
  //   // titles = ["Консул", "Полководець"]
  //   console.log( titles[0] ); // Консул
  //   console.log( titles[1] ); // Полководець
  //   console.log( titles.length ); // 2
  // }
  
  // showName("Юлій", "Цезар", "Консул", "Полководець");