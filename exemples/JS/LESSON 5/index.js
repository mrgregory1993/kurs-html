"use strict";
///////////////////////////Object. Properties in object.////////////////
// const title = "asdasdads";
// const oldPrice = 29999;
// const newPrice = 19999;
// const color = ["grey", "black"];
// const = imgLink: "image",

// как создать обьект//

// 1 variant-global constructor object
// const product = new Object()

// 2 variant-funktion constructor

// 2 variant-object literal
// const product = "asdasd" // - строчный литерал
// const product = 14 // - цифровой литерал
// const product = {}; // - обьектный литерал
// const product = {
//   title: "asdasdads",
//   oldPrice: 29999,
//   newPrice: 19999,
//   color: ["grey", "black"],
// };
// console.log(product);

// const product2 = {
// imgLink,
//   title,
//   oldPrice,
//   newPrice,
//   color,
// };
// console.log(product2);

// const product = {
//   imgLink: "image",
//   title: "asdasdads",
//   oldPrice: 29999,
//   newPrice: 19999,
//   color: ["grey", "black"],
//   "is discount": true,
// };
// console.log(product);
// console.log(product.oldPrice);
// console.log(product["oldPrice"]);
// console.log(product["is discount"]);
// product["top sales"] = true; //добавить свойство в обьект
// delete product["is discount"]; //удалить свойство в обьекте
// //property in object
// console.log("imgLink" in product); //проверить свойство в обьектe

///////////////////////////////Object. Objects methods. Setter, getter.//////////

// const product = {
//   imgLink: "image",
//   title: "asdasdads",
//   oldPrice: 29999,
//   _newPrice: 19999,
//   setNewPrice(value) {
//     if (value > 0) {
//       this.newPrice = value;
//     }
//   },
//   color: ["grey", "black"],
//   "is discount": true,

//   set newPrice (value) {
//     if (value > 0) {
//       this._newPrice = value;
//     }
//   },

//   get newPrice () {
//     return this._newPrice;
//   },

// getPriseDiff: function () {
//   return this.oldPrice - this.newPrice;
// },
// loggingThis() {
//   console.log(this); //////////////МЕТОД///////////////
// },
// };

// product.setNewPrice(300);
// console.log(product.newPrice);

// console.log(product.getPriseDiff());
// console.log(this);

// product.newPrice = -500;
// console.log(product.newPrice);

// // product.loggingThis();
// console.log(product);

//!!!!!!!!!!!!!!!!!!!!!!Objects. Copying object. Global Object methods.!!!!!!!!!!!!!!!!!!!!!//

// const product = {
//   title: "Very good skiils",
//   cost: 500,
//   "expiration date": "forever",
//   getExpirationDate() {
//     return product["expiration date"];
//   },
// };

// const product2 = product

// product2.title = "Super good skills"
// console.log(product2.title);
// console.log(product.title);

///////////как скопировать обьект var1//

// const product2 = {}
//   for (const key in product) {
//     product2[key] = product[key]
//     }
// product2.title = "Super good skills"
// console.log(product2.title);
// console.log(product.title);

/////////////как скопировать обьект var2 (метод глобального обьекта//

// const product2 = Object.assign({}, product)

// product2.title = "Super good skills"
// console.log(product2.title);
// console.log(product.title);

/////////////////////методы обьектов (глобальные)/////////////////////////////////

// console.log(Object.assign({name: 1}, {name: 2}));
// console.log(Object.assign({name: 1},{title:"asdad"}, {name: 2}, {date:"2020"}));
// console.log(Object.keys(product));
// console.log(Object.values(product));
// console.log(Object.entries(product));
// console.log(Object.defineProperty(product,"title", {
//   writable: false
// }));

// Object.defineProperty(product, "title", {
// writable: false,
// enumerable: true,
// });
// product.title = "Super good skills"

// Object.defineProperty(product, "bigTitle", {////////////создаем новое свойство bigTitle////////
//   writable: false,
//   enumerable: true,
//   value: "VERY GOOD SKILLS"
// });

// product.title = "Super good skills"

// for (const key in product) {
//   console.log(key);
// }

/////////////////////методы обьектов (для всех)/////////////////////////////////

// console.log(product.hasOwnProperty("titles"));
// ///or///
// console.log("titles" in product);

//******************************ЗАДАЧИ*******************************//

////////////////Задача 3
// Скопируйте объект student из задачи 1, напишите после его создания такой код: если коэффициент лени больше или равен 3, и при этом меньше или равен 5, а коэффициент хитрости меньше или равен 4 - вывести в консоль сообщение "Cтудент имяСтудента фамилияСтудента отправлен на пересдачу".

// const student = {
//   name:"Pidor",
//   "last name":"Govnojuy",
//   laziness: 4,
//   trick:5,

// }
// console.log(student);
// student.trick = 3
// if (student.laziness >=3 && student.laziness<=5 && student.trick<=4) {
//   console.log(`Cтудент ${student.name} ${student["last name"]} отправлен на пересдачу` );
// }

/////////////////////////////Задача 7
// Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Вывести объект в консоль. После чего спрашивать у пользователя в цикле два вопроса - "Какое свойство вы хотите изменить?", потом - "На какое значение?". Цикл может иметь максимум 3 итерации. Если пользователь нажмет Cancel на любой из вопросов - досрочно прервать цикл. Вывести объект в консоль.
// const danItStudent = {
//   name: "Pidor",
//   "last name": "Govnojuy",
//   homework: 4,
// };
// console.log(danItStudent);
// for (let i = 0; i < 3; i++) {
//   let question1 = prompt("Какое свойство вы хотите изменить?");
//   let question2 = prompt("На какое значение?");
//   if (
//     question1 === null ||
//     question1 === "" ||
//     question2 === null ||
//     question2 === ""
//   ) {
//     break;
//   }
//   danItStudent[question1] = question2;
// }

// console.log(danItStudent);

/////////////answer/////////////////////

// const danItStudent = {
//   name: "Мирослав",
//   "last name": "Полищук",
//   homeworks: 30
// };

// console.log(danItStudent);

// for(let i = 0; i < 3; i++) {
//   let property = prompt("Какое свойство вы хотите изменить?");
//   let newValue = prompt("На какое значение?");

//   if(!property || !newValue) {//????????????????????????зачем??
//       break;
//   }

//   if(property === "homeworks") {//?????????????????????зачем??
//       newValue = +newValue;
//   }

//   danItStudent[property] = newValue;
// }

// console.log(danItStudent);


//////////////////пример

// const person = {
//   name: "Рокэ",
//   "last name": "Алва",
//   "eyes color": "сапфировые",
//   "hair color": "черные",
//   "семейный статус": "НИКОГДА",
//   "право на трон": undefined,
//   wife: "Bitch",
// }

// if (person["право на трон"] === undefined) { // неправильное условие, потому что свойство "право на трон" есть, просто оно равно undefined
//   console.log("Рокэ Алва не имеет никаких прав на трон!"); // на самом деле нет, права есть, их статус неизвестен
// } 

// if ("право на трон" in person) { // правильное условие, возвращает true, если свойство с таким имененм вообще есть у объекта, даже если его значение равно undefined
//   console.log("У Рокэ Алва есть права на трон!"); // права есть, их статус неизвестен
// } 

// if (!("wife" in person)) { // такие инструкции для получения их обратного значения через оператор отрицания ! нужно заключать в круглые скобки. "wife" in person вернет false, который мы превратим в true для вывода нужного в таком случае сообщения, обнадеживающего потенциальных невест
//   console.log("У Первого маршала Талига нет жены! У вас еще есть шанс!")
// }
// else{
//   console.log(person.wife)
// }


//////////////////////////Задача 1
// Создайте объект notebook, у которого будут такие свойства: название (name), цена (price) и размер (size). Поле size должно юыть объектом с такими свойствами: ширина (width), высота (height) и толщина (depth). Выведите в консоль сообщение: Ноутбук {имяНоутбкука}. Стоит {ценаНоутбука} грн. Размеры: высота - {высотаНоутбука}, ширина - {ширинаНоутбука}, толщина - {толщинаНоутбука}.

/////////////////////////////Задача 2
// Выведите в консоль все размеры нотбука отдельными сообщениями, используя функцию перебора for..in: {имяСвойства} равна {ширинаНоутбука}, {имяСвойства} равна {высотаНоутбука}, {имяСвойства} равна {толщинаНоутбука}.

// Нажмите, чтобы подсмотреть решение
// let notebook = {
//   name: "Lenovo",
//   price: 20000,
//   size: {
//     width: "300 мм",
//     height:" 250 мм",
//     depth: "35 мм"
//   },
// }
// console.log(`Ноутбук ${notebook.name}. Стоит ${notebook.price} грн. Размеры: высота -  ${notebook.size.width}, ширина -  ${notebook.size.height}, толщина -  ${notebook.size.depth}`);
// for (const key in notebook.size) {
//   console.log(`${key} равна ${notebook.size[key]}`);
//   }
